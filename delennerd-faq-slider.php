<?php
/**
 * Plugin Name: Delennerd FAQ Slider
 * Description: Delennerd FAQ Slider
 * Version:     1.1.1
 * Author:      Pascal Lehnert
 * Author URI:  https://delennerd.de
 * Text Domain: delennerd-faq-slider
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'DLM_FAQ_NAME', 'Delennerd FAQ Slider' );
define( 'DLM_FAQ_PREFIX', 'dlm_faq' );
define( 'DLM_FAQ_LOCALE_PREFIX', 'dlm-faq-slider' );
define( 'DLM_FAQ_VER', '1.1.0' );

define( 'DLM_FAQ__FILE__', __FILE__ );
define( 'DLM_FAQ_PATH', plugin_dir_path( DLM_FAQ__FILE__ ) );
//define( 'DLM_FAQ_BASE', plugin_basename( DLM_FAQ__FILE__ ) );
define( 'DLM_FAQ_URL', plugins_url( '/', DLM_FAQ__FILE__ ) );
define( 'DLM_FAQ_ASSETS_PATH', DLM_FAQ_PATH . 'assets/' );
define( 'DLM_FAQ_WIDGETS_PATH', DLM_FAQ_PATH . 'widgets/' );
define( 'DLM_FAQ_ASSETS_URL', DLM_FAQ_URL . 'assets/' );
define( 'DLM_FAQ_WIDGETS_URL', DLM_FAQ_URL . 'widgets/' );

require DLM_FAQ_PATH . '/inc/update-checker.php';
require DLM_FAQ_PATH . '/inc/adminpanel.php';
require DLM_FAQ_PATH . '/inc/DlmFaqSlider.class.php';

function dlm_faq_enqueue_scripts() {
    $css_version = DLM_FAQ_VER . '.' . filemtime( DLM_FAQ_ASSETS_PATH . 'css/dlm-faq-slider.min.css' );
    wp_register_style( 'dlm-faq-slider-style', DLM_FAQ_ASSETS_URL . 'css/dlm-faq-slider.min.css', array(), '' );

    $js_version = DLM_FAQ_VER . '.' . filemtime( DLM_FAQ_ASSETS_PATH . 'js/dlm-faq-slider.js' );
    wp_register_script( 'dlm-faq-slider-scripts', DLM_FAQ_ASSETS_URL . 'js/dlm-faq-slider.js', array(), '' );
}

add_action( 'wp_enqueue_scripts', 'dlm_faq_enqueue_scripts' );


if (!function_exists( 'DLM_FAQ_shortcode' )) {
    function DLM_FAQ_shortcode($atts = [], $content = null)
    {
        $atts = array_change_key_case((array)$atts, CASE_LOWER);
        $dlmFaqSlider = new DlmFaqSlider($atts);

        wp_enqueue_style('dlm-faq-slider-style');
        wp_enqueue_script('dlm-faq-slider-scripts');
        
        return $dlmFaqSlider->init();
    }

    add_shortcode( 'dlm-faq-slider', 'DLM_FAQ_shortcode' );
}