<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

require_once( DLM_FAQ_PATH . '/vendor/autoload.php' );

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon_Fields\Field\Complex_Field;


class DlmFaqAdminPanel {
    public function __construct()
    {
        add_action( 'after_setup_theme', [ $this, 'loadOptionFields' ] );

        add_action( 'carbon_fields_register_fields', [ $this, 'addPluginSettingsPage' ] );
    }

    function loadOptionFields() 
    {
        \Carbon_Fields\Carbon_Fields::boot();
    }

    function addPluginSettingsPage()
    {
        $questions_labels = array(
            'plural_name' => 'Questions',
            'singular_name' => 'Question',
        );

        $dlm_options_container = Container::make( 'theme_options', __( DLM_FAQ_NAME, DLM_FAQ_LOCALE_PREFIX ) )
            ->set_icon( 'dashicons-book' )
            ->set_page_file( DLM_FAQ_LOCALE_PREFIX )
            ->add_fields( array(
                Field::make( 'complex', 'dlm_faqs', 'FAQs' )
                    // ->setup_labels( $questions_labels )
                    ->set_layout('tabbed-vertical')
                    ->set_duplicate_groups_allowed(true)
                    ->add_fields( array(

                        Field::make( 'radio', 'enabled', __( 'Show video', DLM_FAQ_LOCALE_PREFIX ) )
                            ->add_options( array(
                                'yes' => __( 'Show' ),
                                'no' => __( 'Hide' ),
                            ) ),

                        Field::make( 'text', 'question', 'Question', DLM_FAQ_LOCALE_PREFIX ),

                        Field::make( 'radio', 'video_mode', __( 'Video mode', DLM_FAQ_LOCALE_PREFIX ) )
                            ->add_options( array(
                                'portrait' => __( 'Portrait' ),
                                'landscape' => __( 'Landscape' ),
                            ) ),

                        
                        Field::make( 'select', 'video_source', __( 'Video Source', DLM_FAQ_LOCALE_PREFIX ) )
                            ->add_options( array(
                                'youtube' => __( 'Youtube' ),
                                'vimeo' => __( 'Vimeo' ),
                                'wp_media' => __( 'WordPress' ),
                                'self_hosted' => __( 'Self hosted' ),
                            ) ),

                        Field::make( 'text', 'video_url', __('YouTube or Vimeo Video-ID', DLM_FAQ_LOCALE_PREFIX ) )
                            ->set_conditional_logic( array(
                                    'relation' => 'OR',
                                    array(
                                        'field' => 'video_source',
                                        'value' => 'youtube',
                                    ),
                                    array(
                                        'field' => 'video_source',
                                        'value' => 'vimeo',
                                    ),
                            ) ),

                        Field::make( 'file', 'video_media_url', __( 'Video file', DLM_FAQ_LOCALE_PREFIX ) )
                            ->set_type( array( 'video' ) )
                            // ->set_value_type( 'url' )
                            ->set_conditional_logic( array(
                                array(
                                    'field' => 'video_source',
                                    'value' => 'wp_media',
                                ),
                            ) ),

                        Field::make( 'text', 'video_selfhosted_url', __( 'Video URL', DLM_FAQ_LOCALE_PREFIX ) )
                            ->set_attribute( 'type', 'url' )
                            ->set_attribute( 'placeholder', 'https://url-zur-datei.de/video.mp4' )
                            ->set_conditional_logic( array(
                                array(
                                    'field' => 'video_source',
                                    'value' => 'self_hosted',
                                ),
                            ) ),

                        Field::make( 'image', 'image', __('Preview image', DLM_FAQ_LOCALE_PREFIX ) ),
                    ) )
                    ->set_header_template( '
                        <% if (question) { %>
                            <%- question %>
                        <% } %>
                    ' ),
            ) );

        Container::make( 'theme_options', __( 'About', DLM_FAQ_LOCALE_PREFIX ) )
            ->set_page_parent( $dlm_options_container )
            ->set_page_file( DLM_FAQ_LOCALE_PREFIX . '-about' )
            ->add_fields( array(
                Field::make( 'html', DLM_FAQ_PREFIX . '_information_text' )
                    ->set_html( $this->aboutPageContent() )
            ) );
    }

    private function aboutPageContent()
    {
        $html = '
            <h3>About</h3>
            <p>Das Plugin <b>' . DLM_FAQ_NAME . '</b> wurde von Pascal Lehnert programmiert.</p>

            <h3>Support</h3>
            <p>Bei Fragen oder Problemen, wenden Sie sich bitte an <a href="mailto:mail@delennerd.de?subject=Problem mit '. DLM_FAQ_NAME .'&body=Hallo Pascal, ich habe eine Frage zu deinem FAQ Slider.">Pascal Lehnert</a>.</p>
        ';

        return $html;
    }
}

new DlmFaqAdminPanel();