<?php
/**
 * DlmFaqSlider
 * 
 * @package delennerd-faq-slider
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

class DlmFaqSlider {
    private $moduleName = 'faq-slider';
    private $atts;
    private $postType = "faqs";

    function __construct($atts) {
        $this->atts = $atts;
    }

    public function init() 
    {
        return $this->generateLoop();
    }

    /**
     * Generates the entries loop
     */
    private function generateLoop() {
        $faqs = carbon_get_theme_option('dlm_faqs');

        ob_start();

        ?>

        <div class="dlm-faq-slider">

            <div class="loading">
                <img src="<?php echo DLM_FAQ_ASSETS_URL ?>images/loading.svg" alt="">
            </div>

            <?php

            include DLM_FAQ_PATH . "template-parts/header.php";

            if ( count($faqs) > 0 ) :

                for ( $index=0; $index < count($faqs); $index++ ) :

                    if ($faqs[$index]['enabled'] !== 'yes') continue;

                    include DLM_FAQ_PATH . "template-parts/loop.php";

                endfor;
                // end of loop

            endif;

            include DLM_FAQ_PATH . "template-parts/footer.php";

            include DLM_FAQ_PATH . "template-parts/popup.php";

            ?>

        </div><!-- end .items -->

        <?php

        $return = ob_get_contents();
        ob_get_clean();

        return $return;
    }
}