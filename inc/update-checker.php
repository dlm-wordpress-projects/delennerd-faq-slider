<?php
/**
 * Update Checker
 * 
 * @package delennerd-faq-slider
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

require_once DLM_FAQ_PATH . '/plugin-update-checker/plugin-update-checker.php';

$dlmUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://update.kunden-webseite.de/wordpress/plugins/delennerd-faq-slider/info.json',
	DLM_FAQ_PATH . '/delennerd-faq-slider.php',
	'delennerd-faq-slider'
);