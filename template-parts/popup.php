<?php
/**
 * template-parts/popup
 * 
 * @package delennerd-faq-slider
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$question = "question";
$image = "image";
$video_url = "video_url";
$videomode = 'video_mode';

?>

<div class="modal fade dlmVideoPopup" id="dlmVideoPopup" data-backdrop="false" data-keyboard="false" tabindex="-1"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                <div class="dlmVideosWrapper">
                    <div class="dlmVideosWrapper__topbar"></div>

                    <div class="dlmVideoCarousel">

                        <?php for ($index=0; $index < count($faqs); $index++ ) : ?>

                            <?php if ($faqs[$index]['enabled'] !== 'yes') continue; ?>

                            <div data-title="<?php echo $faqs[$index][$question] ?>">
                                <div class="dlmVideosWrapper__item video-<?php echo $faqs[$index][$videomode] ?>">
                                    <div class="inner">
                                        <div class="item_video">

                                            <div class="dlmPlayer">

                                            <?php if ($faqs[$index]['video_source'] === 'vimeo') : ?>

                                                <div 
                                                    class="dlmJsPlayer" 
                                                    playsinline
                                                    data-plyr-provider="<?php echo $faqs[$index]['video_source'] ?>"
                                                    data-plyr-embed-id="<?php echo $faqs[$index][$video_url] ?>"
                                                ></div>

                                            <?php endif; ?>

                                             <?php if ($faqs[$index]['video_source'] === 'youtube') : ?>

                                                <div class="plyr__video-embed dlmJsPlayer">
                                                    <iframe
                                                        src="https://www.youtube.com/embed/<?php echo $faqs[$index][$video_url] ?>?iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
                                                        crossorigin
                                                        allowtransparency
                                                        allow="autoplay"
                                                    ></iframe>
                                                </div>

                                            <?php else: ?>

                                                <?php 
                                                $videoUrl = '';

                                                if ($faqs[$index]['video_source'] === 'wp_media') {
                                                    $videoFile = parse_url( wp_get_attachment_url( $faqs[$index]['video_media_url'] ) );
                                                    $videoUrl = $videoFile['path'];
                                                }

                                                if ($faqs[$index]['video_source'] === 'self_hosted') {
                                                    $videoUrl = $faqs[$index]['video_selfhosted_url'];
                                                }

                                                ?>

                                                <video 
                                                    class="dlmJsPlayer" 
                                                    playsinline 
                                                    data-poster="<?php echo wp_get_attachment_image_src($faqs[$index][$image], 'medium_large')[0] ?>">
                                                    <source src="<?php echo $videoUrl; ?>" type="video/mp4" />
                                                </video>

                                            <?php endif; ?>

                                            </div>

                                        </div><!-- end .item_video -->

                                        <div class="item__text">
                                            <div class="item__textInner">
                                                <p><?php echo $faqs[$index][$question] ?></p>

                                                <footer class="info-text has-icon icon-star">
                                                    Info Text Message
                                                </footer>
                                            </div>
                                        </div><!-- end .item__text -->
                                    </div><!-- end .inner -->
                                </div><!-- end .dlmVideosWrapper__item -->
                            </div>

                        <?php endfor; ?>

                    </div><!-- end .dlmVideoCarousel -->

                </div><!-- end .videoWrapper -->

            </div>
        </div>
    </div>
</div>