<?php
/**
 * template-parts/loop
 * 
 * @package delennerd-faq-slider
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$question = "question";
$image = "image";
$video_url = "video_url";
$videomode = 'video_mode';
$enabled = 'enabled';

?>

<div>
    <div class="faq-box">

        <div class="faq-box__image__wrapper"
            style="background-image: url('<?php echo wp_get_attachment_image_src($faqs[$index][$image], 'medium_large')[0] ?>')">
        </div>

        <div class="faq-box__overlay">

            <div class="faq-box__button" data-title="<?php echo $faqs[$index][$question] ?>">
                <button class="btn btn-secondary btn-block open-modal" data-target="#dlmVideoPopup">
                    <?php echo $faqs[$index][$question] ?>
                </button>

                <button class="videoBtn videoBtn-play open-modal" data-target="#dlmVideoPopup" aria-label="Wiedergabe"
                    role="button" tabindex="0">
                    <svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path
                            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z">
                        </path>
                    </svg>
                </button>
            </div>


        </div>

    </div>
</div>